public class Block {
  
  // Constants
  private static final int BLOCKSIZE = 55, SPACING = 4;
  private static final int FIRST_BLOCK_X = 5, FIRST_BLOCK_Y = 110;
  
  // Attributes 
  private int row, col;      // Position of the block 
  private int number;        // Number on the block
  private color blockColor;  // Color of the block
  
  // Constructors
  public Block(int row, int col) {
    this.row = row;
    this.col = col;
    this.blockColor = color(245, 40, 112);
  }
  public Block(int row, int col, int number) {
    this(row, col);
    this.number = number;
  }
  
  // Getters & Setters
  public int getRow() {
    return row;
  }
  public int getCol() {
    return col;
  }
  public void moveDownRow() {
    col++;
  }
  public void decNumber() {
    number--;
  }
  public void setBlockColor(color blockColor) {
    this.blockColor = blockColor;
  }
  
  
  // ----------------------------------------------------
  //   DRAW
  // ----------------------------------------------------
  
  void draw() {
    // Draw the block
    fill(blockColor);
    stroke(blockColor);
    rect(row * (SPACING + BLOCKSIZE) + FIRST_BLOCK_X, col * (SPACING + BLOCKSIZE) + FIRST_BLOCK_Y, BLOCKSIZE, BLOCKSIZE);
    // Write the number
    fill(0);
    textSize(20);
    textAlign(CENTER);
    text(str(number), FIRST_BLOCK_X + row * (BLOCKSIZE + SPACING) + BLOCKSIZE / 2, 
                      FIRST_BLOCK_Y + col * (BLOCKSIZE + SPACING) + BLOCKSIZE / 2 + 7); 
    
  }
  
  
  
  
  
  
}
