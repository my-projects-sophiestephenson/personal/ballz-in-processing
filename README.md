## A recreation of the popular app Ballz using Processing 
** No longer being worked on - see [Ballz in Processing Part 2](https://gitlab.com/my-projects-sophiestephenson/personal/ballz-in-processing-part-2) **

My goal for this project was to get more experience using Processing with a more 
challenging game. The object of the game is to hit the blocks with the ball and 
prevent blocks from reaching the bottom of the board. The app uses a pull-and-aim 
type method to shoot the ball, but I changed that to arrow keys so it worked
better on the computer. To play, follow these steps:

1. Download Processing if needed [here](https://processing.org/download/)
2. Download all .pde files and save in a folder called Ballz
3. Open ballz.pde in Processing and hit run