Board board = new Board();

void setup() {
  size(420, 570);
  background(0);
  
}

void draw() {
  board.draw();
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      board.moveLine(1);
      //board.makeNewRow();    // For testing purposes only
    }
    else if (keyCode == DOWN)
      board.moveLine(2);
    else if (keyCode == LEFT)
      board.moveLine(3);
    else if (keyCode == RIGHT)
      board.moveLine(4);
  } else if (key == 10)
      board.shootBall();
  
}
