import java.util.ArrayList;

public class Board {
  
  // Constants
  private static final int BOARDSIZE = 7, BOARDSTART = 60;
  private static final int BOARD_WIDTH = 420, BOARD_HEIGHT = 502;
  private static final int BALL_WIDTH = 15, BALL_COLOR = 100;
  private static final int BALL_STARTX = 210, BALL_STARTY = 562;
  private static final double LINE_MOVE_SPEED = 30, BALL_MOVE_SPEED = 100;
  private static final int UP = 1, DOWN = 2, LEFT = 3, RIGHT = 4;
  
  // Attributes
  private float ballX, ballY;       // Coordinates of the center of the ball
  private float lineEndX, lineEndY; // Where the line is pointing
  private float lineAngle;          // Angle of the line with relation to the left side of the bottom (radians)
  private int numBalls;             // Number of balls currently in the game
  private boolean ballIsMoving;     // Whether the ball is currently moving
  private ArrayList<Block> blocks;  // The blocks on the board
  
  // Constructor
  public Board() {
    ballX = BALL_STARTX;             // Set starting ball coordinates
    ballY = BALL_STARTY;         
    numBalls = 1;                    // Start with a single ball
    ballIsMoving = false;            // Ball starts stationary
    lineEndX = BALL_STARTX;          // Line should start pointing straight up
    lineEndY = BOARDSTART;           // It goes up until it hits the end of the board
    lineAngle = 0;                   // Line angle starts at 0 (straight up)
    blocks = new ArrayList<Block>(); // Initialize block ArrayList 
    //makeNewRow();                    // Start by making a new row of blocks at the top
  }
  
  // Getters & Setters
  public float getBallX() {
    return ballX;
  }
  public void setBallX(int ballX) {
    this.ballX = ballX;
  }
  public float getBallY() {
    return ballY;
  }
  public void setBallY(int ballY) {
    this.ballY = ballY;
  }
  public int getNumBalls() {
    return numBalls;
  }
  public void incNumBalls() {
    numBalls++;
  }
  public ArrayList<Block> getBlocks() {
    return blocks; 
  }
  
  // ----------------------------------------------------
  //   METHODS
  // ----------------------------------------------------
  
  // Moves each block down one row and adds a new row of blocks at the top
  public void makeNewRow() {
    // Go thru and move each block down a row
    ArrayList<Block> blocksToRemove = new ArrayList<Block>();
    for (Block block : blocks) {
      block.moveDownRow();
      if (block.getCol() >= BOARDSIZE) {
          // End Game
          blocksToRemove.add(block);
      }
    }
    // Remove blocks that are too far down (just until End Game is done)
    for (Block block : blocksToRemove) 
      blocks.remove(block);
    
    // Decide on a random number of new blocks to add
    int numNewBlocks = (int) random(BOARDSIZE) + 1;
    
    // Make a list of possible columns the new blocks could be in
    ArrayList<Integer> possibleCols = new ArrayList<Integer>();
    for (int i = 0; i < BOARDSIZE; i++) 
      possibleCols.add(i);
      
    // Pick random columns from the list and add blocks in those spots
    for (int i = 0; i < numNewBlocks; i++) {
      int col = (int) random(possibleCols.size());
      float rand = random(1);
      int newNum = numBalls;
      if (rand > 0.8)
        newNum *= 2;
      blocks.add(new Block(col, 0, newNum));
      possibleCols.remove(col);
    }
  
    // Increment the number of balls
    incNumBalls();
  }
  
  // Shoots the ball
  public void shootBall() {
    ballIsMoving = true;
    while (ballIsMoving) {
      float distX = Math.abs(ballX - lineEndX), distY = Math.abs(ballY - lineEndY);
      while (distX > BALL_WIDTH && distY > BALL_WIDTH) {
          
      }
      
      
    }
    makeNewRow();
  }
  
  // Moves the line in a certain direction :: Shorten when time
  public void moveLine(int dir) {
    switch (dir) {
      case LEFT :  {if (lineEndX < BALL_STARTX && lineEndY >= BALL_STARTY - 5)
                        return;
                    if (lineEndX <= 0 && !(lineEndY >= BALL_STARTY)) 
                      lineEndY += LINE_MOVE_SPEED;
                    else if (lineEndY <= BOARDSTART)
                      lineEndX -= LINE_MOVE_SPEED;
                    else if (lineEndX >= 0)
                      lineEndY -= LINE_MOVE_SPEED;
                    break;}
      case RIGHT:  {if (lineEndX > BALL_STARTX && lineEndY >= BALL_STARTY - 5)
                        return;
                    if (lineEndX >= BOARD_WIDTH && !(lineEndY >= BALL_STARTY)) 
                      lineEndY += LINE_MOVE_SPEED;
                    else if (lineEndY <= BOARDSTART)
                      lineEndX += LINE_MOVE_SPEED;
                    else if (lineEndX <= BOARD_WIDTH)
                      lineEndY -= LINE_MOVE_SPEED;
                    break;}
                   
      case UP:   {if (lineEndX <= 0 && lineEndY > BOARDSTART)
                      lineEndY -= LINE_MOVE_SPEED;
                  else if (lineEndX < ballX)
                      lineEndX += LINE_MOVE_SPEED;
                  else if (lineEndX >= BOARD_WIDTH && lineEndY > BOARDSTART)
                      lineEndY -= LINE_MOVE_SPEED;
                  else if (lineEndX > ballX)
                      lineEndX -= LINE_MOVE_SPEED;
                  break;} 
      case DOWN: {if (lineEndY >= BALL_STARTY - 5)
                      return;
                  else if (lineEndX <= 0)
                      lineEndY += LINE_MOVE_SPEED;
                  else if (lineEndX < ballX)
                      lineEndX -= LINE_MOVE_SPEED;
                  else if (lineEndX >= BOARD_WIDTH)
                      lineEndY += LINE_MOVE_SPEED;
                  else if (lineEndX > ballX)
                      lineEndX += LINE_MOVE_SPEED;
                  break;}
    }
    println("Line End X: " + lineEndX + "   Line End Y: " + lineEndY);
    
    
  }
  
  // ----------------------------------------------------
  //   DRAW
  // ----------------------------------------------------

  void draw() {
      // Board background
      rect(0, 60, 420, 570);
      // Draw blocks
      for (Block block : blocks) {
        block.draw(); 
      }
      
      // Draw ball
      fill(BALL_COLOR);
      stroke(BALL_COLOR);
      ellipse(ballX, ballY, BALL_WIDTH, BALL_WIDTH);
      // Draw the line, if the ball isn't moving
      if (!ballIsMoving) {
        strokeWeight(1);
        line(ballX, ballY, lineEndX, lineEndY);
      }
      
      // Header with text
      fill(50);
      stroke(50);
      rect(0, 0, 420, 60);
      fill(0);
      stroke(0);
      textAlign(CENTER);
      textSize(30);
      text(str(numBalls), 210, 40);
  }
  
  
}
